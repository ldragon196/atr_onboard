# Environment
ESP-IDF v4.3.2

# Module SIM800 connection
Change the defination of UART pins in the config.h file

    /* GSM module */
    #define MODEM_UART_NUM                                UART_NUM_1
    #define MODEM_UART_TX_PIN                             25
    #define MODEM_UART_RX_PIN                             26
    #define MODEM_UART_RTS_PIN                            -1
    #define MODEM_UART_CTS_PIN                            -1

    /* GPS */
    #define GPS_UART_NUM                                  UART_NUM_2
    #define GPS_UART_RX_PIN                               23

    /* Motor */
    #define MOTOR_MOWER_PIN                               2
    #define MOTOR1_UART_TX_PIN                            16
    #define MOTOR1_UART_RX_PIN                            17
    #define MOTOR2_UART_TX_PIN                            18
    #define MOTOR2_UART_RX_PIN                            19

# Use PPP authen
Comment line 41 (#define MODEM_PPP_AUTH_NONE) and configure name and password

    #define MODEM_PPP_AUTH_USERNAME                   "esp32-idf"
    #define MODEM_PPP_AUTH_PASSWORD                   "esp32"

# MQTT message

{
  "ntrip" : "set",
  "host" : "3.23.52.207",
  "port" : 2101,
  "mountpoint" : "/MGPLUS",
  "username" : "rtk@rospers.com",
  "password" : ""
}

{
  "ota":"update",
  "url":"http://192.168.1.100:8884/firmware.bin",
  "version":1
}

Topic
MQTT_COMMAND_TOPIC                            "gps/command/"
MQTT_OTA_TOPIC                                "device/ota"

MQTT_HEARTBEAT_TOPIC                          "device/heartbeat"
MQTT_GPS_TOPIC                                "gps/status/{device_id}"
MQTT_LOG_TOPIC                                "device/log/{device_id}"