/*
 *  mqtt_api.c
 *
 *  Created on: Jun 7, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <esp_log.h>
#include <mqtt_client.h>
#include <esp_wifi.h>
#include "config.h"
#include "log.h"
#include "mqtt_api.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

/*!
 * @brief  MQTT topic with callback function
 */
typedef struct {
    char topic[MQTT_TOPIC_MAX_LENGTH];
    mqtt_handle_t handler;
} topic_map_t;

/*!
 * @brief  MQTT message struct when using queue
 */
typedef struct {
    char message[MQTT_DATA_MAX_LENGTH];
    char topic[MQTT_TOPIC_MAX_LENGTH];
} mqtt_message_t;

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "MQTT"
#define LOG_LEVEL                                  LOG_LEVEL_MQTT

static esp_mqtt_client_handle_t mqtt_client;
static bool mqtt_broker_connected = false;
static char gateway_id[MQTT_CLIENT_ID_LENGTH] = "ESP-12345678";
static topic_map_t topic_list[MQTT_MAX_SUBCRIBE_TOPIC];
static char topic_include_gw_id[256];
static uint8_t numb_topic = 0;
static QueueHandle_t mqtt_message_queue;

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/

extern const uint8_t ca_cert_pem_start[] asm("_binary_ca_cert_crt_start");
extern const uint8_t ca_cert_pem_end[] asm("_binary_ca_cert_crt_end");

/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static esp_err_t mqtt_client_event_handler(esp_mqtt_event_handle_t event);
static void mqtt_handle_message_task(void* arg);

/******************************************************************************/

/*!
 * @brief  MQTT event handler
 * @param  None
 * @retval None
 */
static esp_err_t mqtt_client_event_handler(esp_mqtt_event_handle_t event)
{
    mqtt_client = event->client;

    switch(event->event_id){
    case MQTT_EVENT_CONNECTED:
        mqtt_broker_connected = true;
        LOG_INFO("MQTT_EVENT_CONNECTED");
        for(uint8_t i = 0; i < numb_topic; i++){
            esp_mqtt_client_subscribe(mqtt_client, topic_list[i].topic, 0);
            LOG_INFO("MQTT subcribe topic %s", topic_list[i].topic);
        }
        break;

    case MQTT_EVENT_DISCONNECTED:
        mqtt_broker_connected = false;
        LOG_INFO("MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        LOG_INFO("MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;

    case MQTT_EVENT_UNSUBSCRIBED:
        LOG_INFO("MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;

    case MQTT_EVENT_PUBLISHED:
        LOG_INFO("MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;

    case MQTT_EVENT_DATA:
        LOG_INFO("MQTT_EVENT_DATA");
        if((event->data_len > 0) && (event->topic_len > 0)){
            LOG_INFO("-------- TOPIC = %.*s", event->topic_len, event->topic);
            mqtt_message_t message;
            sprintf(message.message, "%.*s", event->data_len, event->data);
            sprintf(message.topic, "%.*s", event->topic_len, event->topic);
            if(xQueueSend(mqtt_message_queue, &message, MQTT_QUEUE_MAX_DELAY_MS) != pdTRUE){
                LOG_WARN("Send to mqtt message queue fail");
            }
        }
        break;

    case MQTT_EVENT_ERROR:
        LOG_INFO("MQTT_EVENT_ERROR");
        break;

    default:
        break;
    }

    return ESP_OK;
}

/*!
 * @brief  MQTT handle message receive task
 * @param  None
 * @retval None
 */
static void mqtt_handle_message_task(void* arg){
    mqtt_message_t message;
    uint32_t message_count = 0;
    bool is_exec;

    /* Start MQTT client */
    LOG_INFO("Start MQTT client");
    esp_mqtt_client_start(mqtt_client);

    while(1){
        /* Wait message receive */
        if(xQueueReceive(mqtt_message_queue, &message, portMAX_DELAY) == pdTRUE){
            is_exec = false;
            message_count++;
            LOG_INFO("-------- %u_DATA %s", message_count, message.message);
            for(uint8_t i = 0; i < numb_topic; i++){
                if(strncmp(message.topic, topic_list[i].topic, strlen(topic_list[i].topic)) == 0){
                    if(topic_list[i].handler != NULL){
                        topic_list[i].handler(message.message, strlen(message.message));
                        is_exec = true;
                    }
                }
            }
            if(!is_exec){
                LOG_WARN("message not handle!!!");
            }
            vTaskDelay(100 / portTICK_RATE_MS);
        }
    }
    vTaskDelete(NULL);
}

/******************************************************************************/

/*!
 * @brief  Client to send a publish message to the broker
 */
bool mqtt_api_publish(const char* topic, const char* data, uint32_t len, bool include_gw){
    if(mqtt_broker_connected){
        if(include_gw){
            sprintf(topic_include_gw_id, "%s/%s", topic, gateway_id);
        }
        else{
            sprintf(topic_include_gw_id, "%s", topic);
        }

        int32_t msg_id = esp_mqtt_client_publish(mqtt_client, topic_include_gw_id, data, len, 0, 0);

#if !QTT_LOG_OUTPUT
        LOG_INFO("Sent publish successful, msg_id = %d", msg_id);
#else
        (void) msg_id;
#endif
        return true;
    }

    return false;
}

/*!
 * @brief  Register callback handle data reveived from topic
 */
bool mqtt_register_callback(char* topic, mqtt_handle_t func) {
    if(numb_topic >= MQTT_MAX_SUBCRIBE_TOPIC){
        return false;
    }

    if(topic == NULL || func == NULL){
        return false;
    }

    /* Register callback */
    topic_list[numb_topic].handler = func;
    snprintf(topic_list[numb_topic].topic, MQTT_TOPIC_MAX_LENGTH, "%s", topic);
    numb_topic++;

    LOG_INFO("Add topic [%s] to subcribe list", topic);
    return true;
}

/*!
 * @brief  MQTT client (transport over TCP) initialization and start
 */
void mqtt_api_init(void){    
    /* Create message queue */
    mqtt_message_queue = xQueueCreate(MQTT_MESSAGE_QUEUE_SIZE, sizeof(mqtt_message_t));
    if(mqtt_message_queue == NULL){
        LOG_ERR("Create message queue fail");
        return;
    }

    /* Get id from mac address */
    uint8_t mac_addr[6];
    ESP_ERROR_CHECK(esp_read_mac(mac_addr, ESP_MAC_WIFI_STA));
    sprintf(gateway_id, "ESP-%02X%02X%02X%02X%02X%02X",mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);

    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = MQTT_BROKER_URI,
        .cert_pem = (const char *) ca_cert_pem_start,
        .skip_cert_common_name_check = true,
        .disable_clean_session = true,
        .client_id = gateway_id,
        .event_handle = mqtt_client_event_handler,
    };

    /* Start mqtt client */
    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);

    /* Creat mqtt handle message task */
    BaseType_t result = xTaskCreate(mqtt_handle_message_task, MQTT_TASK_NAME, MQTT_TASK_SIZE,
                                    NULL, MQTT_TASK_PRIORITY, NULL);
    if(result != pdPASS){
        LOG_ERR("Create mqtt task fail %d", result);
    }
}
