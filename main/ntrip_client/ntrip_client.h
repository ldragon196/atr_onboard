/*
 *  ntrip_client.h
 *
 *  Created on: Jun 18, 2022
 */

#ifndef __NTRIP_CLIENT_H
#define __NTRIP_CLIENT_H

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <stdint.h>

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define NTRIP_TASK_NAME                               "NTRIP"
#define NTRIP_TASK_SIZE                               8192
#define NTRIP_TASK_PRIORITY                           3

#define NTRIP_BUFF_SIZE                               512

#define NTRIP_MESSAGE_LENGTH                          128
#define NTRIP_MESSAGE_QUEUE_SIZE                      8

typedef struct {
    char message[NTRIP_MESSAGE_LENGTH];
} ntrip_message_t;

typedef struct {
    uint16_t port;
    char host[128];
    char mountpoint[128];
    char username[32];
    char password[64];
} ntrip_config_t;

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/



/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

/*!
 * @brief  Send data to http server
 * @param  Data and length
 * @retval None
 */
void ntrip_client_send(uint8_t *data, uint16_t length);

/*!
 * @brief  Configure ntrip client
 * @param  Configuration
 * @retval None
 */
void ntrip_client_configuration(ntrip_config_t *config);

/*!
 * @brief  Start ntrip client
 * @param  None
 * @retval None
 */
void ntrip_client_init(void);

/******************************************************************************/

#endif /* __NTRIP_CLIENT_H */
