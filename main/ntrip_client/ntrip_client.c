/*
 *  ntrip_client.c
 *
 *  Created on: Jun 18, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <esp_err.h>
#include <errno.h>
#include <esp_http_client.h>
#include "config.h"
#include "log.h"
#include "gps_api.h"
#include "ntrip_client.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define NTRIP_CHECK(TAG, condition, action, format, ... ) if ((condition)) {                        \
            LOG_ERR("%s:%d (%s): " format, __FILE__, __LINE__, __FUNCTION__,  ##__VA_ARGS__); \
            action;                                                                                 \
        }

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "NTRIP"
#define LOG_LEVEL                                  LOG_LEVEL_NTRIP

esp_http_client_handle_t ntrip_http = NULL;
static bool is_connected = false;
ntrip_config_t ntrip_config;

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static void ntrip_client_task(void* arg);

/******************************************************************************/

/*!
 * @brief  Ntrip client task
 */
static void ntrip_client_task(void* arg){
    while(1){
        /* Configure host URL */
        esp_http_client_config_t config = {
                .host = ntrip_config.host,
                .port = ntrip_config.port,
                .method = HTTP_METHOD_GET,
                .path = ntrip_config.mountpoint,
                .auth_type = HTTP_AUTH_TYPE_BASIC,
                .username = ntrip_config.username,
                .password = ntrip_config.password
        };

        ntrip_http = esp_http_client_init(&config);
        esp_http_client_set_header(ntrip_http, "Ntrip-Version", "Ntrip/2.0");
        esp_http_client_set_header(ntrip_http, "User-Agent", "NTRIP_Client/2.0");
        // esp_http_client_set_header(ntrip_http, "Connection", "close");

        esp_err_t err = esp_http_client_open(ntrip_http, 0);
        NTRIP_CHECK(TAG, err != ESP_OK, goto _error, "Could not open HTTP connection: %d %s", err, esp_err_to_name(err));
        int32_t length = esp_http_client_fetch_headers(ntrip_http);
        NTRIP_CHECK(TAG, length < 0, goto _error, "Could not connect to caster: %d %s", errno, strerror(errno));
        int32_t status_code = esp_http_client_get_status_code(ntrip_http);
        NTRIP_CHECK(TAG, status_code != 200, goto _error, "Could not access mountpoint: %d", status_code);
        NTRIP_CHECK(TAG, !esp_http_client_is_chunked_response(ntrip_http), goto _error, "Caster did not respond with chunked transfer encoding: content_length %d", length);
        LOG_INFO("Successfully connected to %s:%d%s", ntrip_config.host, ntrip_config.port, ntrip_config.mountpoint);

        char *buffer = malloc(NTRIP_BUFF_SIZE);

        int read_length;
        is_connected = true;
        while(is_connected){
            read_length = esp_http_client_read(ntrip_http, buffer, NTRIP_BUFF_SIZE);
            if(read_length < 0){
                break;
            }

            /* Send to GPS module */
            gps_api_send_bytes((uint8_t *) buffer, read_length);
            vTaskDelay(20 / portTICK_RATE_MS);
        }

        free(buffer);
        // LOG_WARN("Disconnected from %s:%d/%s", ntrip_config.host, ntrip_config.port, ntrip_config.mountpoint);

        _error:
            is_connected = false;
            esp_http_client_close(ntrip_http);
            esp_http_client_cleanup(ntrip_http);
            ntrip_http = NULL;
        vTaskDelay(NTRIP_POLLING_TIME_MS / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

/*!
 * @brief  Send data to http server
 */
void ntrip_client_send(uint8_t *data, uint16_t length){
    if(!is_connected){
        return;
    }

    int sent = esp_http_client_write(ntrip_http, (char *) data, length);
    if(sent < 0){
        LOG_WARN("Send to NTRIP message failed");
        esp_http_client_close(ntrip_http);
        esp_http_client_cleanup(ntrip_http);
        ntrip_http = NULL;
    }
}

/*!
 * @brief  Configure ntrip client
 */
void ntrip_client_configuration(ntrip_config_t *config){
    memcpy(&ntrip_config, config, sizeof(ntrip_config_t));
    is_connected = false;
}

/*!
 * @brief  Start ntrip client
 * @param  None
 * @retval None
 */
void ntrip_client_init(void){
    /* Creat Ntrip client task */
    BaseType_t result = xTaskCreate(ntrip_client_task, NTRIP_TASK_NAME, NTRIP_TASK_SIZE,
                                    NULL, NTRIP_TASK_PRIORITY, NULL);
    if (result != pdPASS) {
        LOG_ERR("Create ntrip client task fail %d", result);
    }

    /* Default configuration */
    ntrip_config.port = NTRIP_CLIENT_PORT;
    sprintf(ntrip_config.host, "%s", NTRIP_CLIENT_HOST);
    sprintf(ntrip_config.mountpoint, "%s", NTRIP_CLIENT_MOUNTPOINT);
    sprintf(ntrip_config.username, "%s", NTRIP_CLIENT_USERNAME);
    sprintf(ntrip_config.password, "%s", NTRIP_CLIENT_PASSWORD);
}