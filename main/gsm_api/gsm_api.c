/*
 *  gsm_api.c
 *
 *  Created on: Jun 7, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>
#include <esp_netif.h>
#include <esp_netif_ppp.h>
#include <esp_modem.h>
#include <esp_modem_netif.h>
#include <esp_log.h>
#include <sim800.h>
#include <driver/gpio.h>
#include "config.h"
#include "log.h"
#include "gsm_api.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/



/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "GSM"
#define LOG_LEVEL                                  LOG_LEVEL_GSM

static EventGroupHandle_t gsm_event_group = NULL;
static const int CONNECT_BIT = BIT0;

static esp_netif_t *esp_netif = NULL;

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static void gsm_api_on_ip_event(void *arg, esp_event_base_t event_base,
                                int32_t event_id, void *event_data);
static void gsm_api_on_ppp_changed(void *arg, esp_event_base_t event_base,
                                    int32_t event_id, void *event_data);
static void gsm_api_modem_event_handler(void *event_handler_arg, esp_event_base_t event_base,
                                        int32_t event_id, void *event_data);

/******************************************************************************/

/*!
 * @brief  GSM modem IP event handler
 */
static void gsm_api_on_ip_event(void *arg, esp_event_base_t event_base,
                                int32_t event_id, void *event_data){
    LOG_INFO("IP event! %d", event_id);
    if(event_id == IP_EVENT_PPP_GOT_IP){
        esp_netif_dns_info_t dns_info;

        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        esp_netif_t *netif = event->esp_netif;

        LOG_INFO("Modem Connect to PPP Server");
        LOG_INFO("--------------");
        LOG_INFO("IP          : " IPSTR, IP2STR(&event->ip_info.ip));
        LOG_INFO("Netmask     : " IPSTR, IP2STR(&event->ip_info.netmask));
        LOG_INFO("Gateway     : " IPSTR, IP2STR(&event->ip_info.gw));
        esp_netif_get_dns_info(netif, 0, &dns_info);
        LOG_INFO("Name Server1: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));
        esp_netif_get_dns_info(netif, 1, &dns_info);
        LOG_INFO("Name Server2: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));
        LOG_INFO("--------------");
        xEventGroupSetBits(gsm_event_group, CONNECT_BIT);

        LOG_INFO("GOT ip event!!!");
    }
    else if(event_id == IP_EVENT_PPP_LOST_IP){
        LOG_INFO("Modem Disconnect from PPP Server");
    }
    else if(event_id == IP_EVENT_GOT_IP6){
        LOG_INFO("GOT IPv6 event!");

        ip_event_got_ip6_t *event = (ip_event_got_ip6_t *)event_data;
        LOG_INFO("Got IPv6 address " IPV6STR, IPV62STR(event->ip6_info.ip));
    }
}

/*!
 * @brief  GSM modem ppp changed event handler
 */
static void gsm_api_on_ppp_changed(void *arg, esp_event_base_t event_base,
                                    int32_t event_id, void *event_data){
    LOG_INFO("PPP state changed event %d", event_id);
    if(event_id == NETIF_PPP_ERRORUSER){
        /* User interrupted event from esp-netif */
        esp_netif_t *netif = *(esp_netif_t**)event_data;
        LOG_INFO("User interrupted event from netif: %p", netif);
    }
}

/*!
 * @brief  GSM modem event handler
 */
static void gsm_api_modem_event_handler(void *event_handler_arg, esp_event_base_t event_base,
                                        int32_t event_id, void *event_data){
    switch(event_id){
    case ESP_MODEM_EVENT_PPP_START:
        LOG_INFO("Modem PPP Started");
        break;
    case ESP_MODEM_EVENT_PPP_STOP:
        LOG_INFO("Modem PPP Stopped");
        break;
    case ESP_MODEM_EVENT_UNKNOWN:
        //LOG_WARN("Unknow line received: %s", (char *)event_data);
        break;
    default:
        break;
    }
}

/******************************************************************************/

/*!
 * @brief  GSM module initialization
 */
esp_netif_t *gsm_api_init(void){
#if !defined(MODEM_PPP_AUTH_NONE)
#if CONFIG_LWIP_PPP_PAP_SUPPORT
    esp_netif_auth_type_t auth_type = NETIF_PPP_AUTHTYPE_PAP;
#elif CONFIG_LWIP_PPP_CHAP_SUPPORT
    esp_netif_auth_type_t auth_type = NETIF_PPP_AUTHTYPE_CHAP;
#endif
#endif

    modem_dte_t *gsm_dte = NULL;
    void *modem_netif_adapter;
    modem_dce_t *dce = NULL;
    gsm_event_group = xEventGroupCreate();

    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = (1ULL << MODEM_POWER_PIN) | (1ULL << MODEM_PWRKEY_PIN);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    /* Turn on the Modem power first */
    gpio_set_level(MODEM_POWER_PIN, 1);
    /* Pull down PWRKEY for more than 1 second according to manual requirements */
    gpio_set_level(MODEM_PWRKEY_PIN, 1);
    vTaskDelay(100 / portTICK_PERIOD_MS);
    gpio_set_level(MODEM_PWRKEY_PIN, 0);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    gpio_set_level(MODEM_PWRKEY_PIN, 1);
    vTaskDelay(6000 / portTICK_RATE_MS);

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, ESP_EVENT_ANY_ID, &gsm_api_on_ip_event, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(NETIF_PPP_STATUS, ESP_EVENT_ANY_ID, &gsm_api_on_ppp_changed, NULL));

    /* Create dte object */
    esp_modem_dte_config_t config = ESP_MODEM_DTE_DEFAULT_CONFIG();
    /* Setup UART specific configuration based on kconfig options */
    config.port_num = MODEM_UART_NUM;
    config.tx_io_num = MODEM_UART_TX_PIN;
    config.rx_io_num = MODEM_UART_RX_PIN;
    config.rts_io_num = MODEM_UART_RTS_PIN;
    config.cts_io_num = MODEM_UART_CTS_PIN;
    config.rx_buffer_size = MODEM_UART_RX_BUFFER_SIZE;
    config.tx_buffer_size = MODEM_UART_TX_BUFFER_SIZE;
    config.pattern_queue_size = MODEM_UART_PATTERN_QUEUE_SIZE;
    config.event_queue_size = MODEM_UART_EVENT_QUEUE_SIZE;
    config.event_task_stack_size = MODEM_UART_EVENT_TASK_STACK_SIZE;
    config.event_task_priority = MODEM_UART_EVENT_TASK_PRIORITY;
    config.line_buffer_size = MODEM_UART_RX_BUFFER_SIZE / 2;
    config.baud_rate = MODEM_UART_BAUDRATE;

    gsm_dte = esp_modem_dte_init(&config);
    /* Register event handler */
    ESP_ERROR_CHECK(esp_modem_set_event_handler(gsm_dte, gsm_api_modem_event_handler, ESP_EVENT_ANY_ID, NULL));

    /* Init netif object */
    esp_netif_config_t cfg = ESP_NETIF_DEFAULT_PPP();
    esp_netif = esp_netif_new(&cfg);
    assert(esp_netif);

    modem_netif_adapter = esp_modem_netif_setup(gsm_dte);
    esp_modem_netif_set_default_handlers(modem_netif_adapter, esp_netif);

    uint8_t try_count = 0;
    do {
        dce = sim800_init(gsm_dte);
        vTaskDelay(10000 / portTICK_PERIOD_MS);

        if(++try_count >= 10){
            LOG_ERR("sim800_init return NULL");
            return NULL;
        }

    } while(dce == NULL);

    ESP_ERROR_CHECK(dce->set_flow_ctrl(dce, MODEM_FLOW_CONTROL_NONE));
    ESP_ERROR_CHECK(dce->store_profile(dce));
    /* Print Module ID, Operator, IMEI, IMSI */
    LOG_INFO("Module: %s", dce->name);
    LOG_INFO("Operator: %s", dce->oper);
    LOG_INFO("IMEI: %s", dce->imei);
    LOG_INFO("IMSI: %s", dce->imsi);
    /* Get signal quality */
    uint32_t rssi = 0, ber = 0;
    ESP_ERROR_CHECK(dce->get_signal_quality(dce, &rssi, &ber));
    LOG_INFO("rssi: %d, ber: %d", rssi, ber);
    /* Get battery voltage */
    uint32_t voltage = 0, bcs = 0, bcl = 0;
    ESP_ERROR_CHECK(dce->get_battery_status(dce, &bcs, &bcl, &voltage));
    LOG_INFO("Battery voltage: %d mV", voltage);

    /* Setup PPPoS network parameters */
#if !defined(MODEM_PPP_AUTH_NONE) && (defined(CONFIG_LWIP_PPP_PAP_SUPPORT) || defined(CONFIG_LWIP_PPP_CHAP_SUPPORT))
        esp_netif_ppp_set_auth(esp_netif, auth_type, MODEM_PPP_AUTH_USERNAME, MODEM_PPP_AUTH_PASSWORD);
#endif

    /* Attach the modem to the network interface */
    esp_netif_attach(esp_netif, modem_netif_adapter);
    /* Wait for IP address */
    xEventGroupWaitBits(gsm_event_group, CONNECT_BIT, pdTRUE, pdTRUE, portMAX_DELAY);
    LOG_INFO("Connected");

    return esp_netif;
}
