/*
 *  motor.h
 *
 *  Created on: Jun 8, 2022
 */

#ifndef _MOTOR_H_
#define _MOTOR_H_

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <driver/gpio.h>

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define MOTOR_UART_RX_BUFFER_SIZE                     256
#define MOTOR_TASK_PERIODS_MS                         50
#define MOTOR_START_FRAME                             0xABCD

typedef enum {
    MOTOR1_ID = 0,
    MOTOR2_ID,
    MOTOR_NUM
} motor_id_t;

typedef struct {
    uint16_t start;
    int16_t  cmd1;
    int16_t  cmd2;
    int16_t  speedR;
    int16_t  speedL;
    int16_t  voltage;
    int16_t  temperature;
    uint16_t led;
    uint16_t checksum;
} motor_feedback_t;

typedef struct {
    uint16_t start;
    int16_t  steer;
    int16_t  speed;
    uint16_t checksum;
} motor_control_t;

typedef struct {
    uint8_t prev_byte;
    uint8_t index;
    uint16_t temp_value;
    uint8_t *pdata;
} motor_reception_t;


#define MOTOR_ENABLE_MOWER()                          gpio_set_level(MOTOR_MOWER_PIN, MOTOR_MOWER_LEVEL)
#define MOTOR_STOP_MOWER()                            gpio_set_level(MOTOR_MOWER_PIN, !MOTOR_MOWER_LEVEL)

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/



/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

/*!
 * @brief  Control motor
 * @param  Steer and speed
 * @retval None
 */
void motor_control(int16_t steer, int16_t speed);

/*!
 * @brief  Motor controller initialization
 * @param  None
 * @retval None
 */
void motor_init(void);

/******************************************************************************/

#endif /* _MOTOR_H_ */
