/*
 *  motor.c
 *
 *  Created on: Jun 7, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>
#include <driver/uart.h>
#include <esp_log.h>
#include "config.h"
#include "log.h"
#include "sw_serial.h"
#include "motor.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/



/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "MOTOR"
#define LOG_LEVEL                                  LOG_LEVEL_MOTOR

motor_feedback_t motor_feedback[MOTOR_NUM];
static motor_reception_t motor_reception[MOTOR_NUM];
static sw_serial_t *sw_serial_motor[MOTOR_NUM];

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static void motor_task(void* arg);

/******************************************************************************/

static void motor_parse_command(uint8_t id, uint8_t byte){
    motor_reception[id].temp_value = (byte << 8) | motor_reception[id].prev_byte;

    /* Initialize if new data is detected */
    if(motor_reception[id].temp_value == MOTOR_START_FRAME){
        motor_reception[id].pdata = (uint8_t *) &motor_feedback[id];
        *motor_reception[id].pdata++ = motor_reception[id].prev_byte;
        *motor_reception[id].pdata++ = byte;
        motor_reception[id].index = 2;
    }

    /* Receiving data */
    else if((motor_reception[id].index >= 2) && (motor_reception[id].index < sizeof(motor_feedback_t))){
        *motor_reception[id].pdata++ = byte;
        motor_reception[id].index++;
    }

    /* Check if we reached the end of the package */
    if(motor_reception[id].index == sizeof(motor_feedback_t)){
        uint16_t checksum;
        checksum = (uint16_t)(motor_feedback[id].start ^ motor_feedback[id].cmd1 ^ motor_feedback[id].cmd2
                            ^ motor_feedback[id].speedR ^ motor_feedback[id].speedL ^ motor_feedback[id].voltage
                            ^ motor_feedback[id].temperature ^ motor_feedback[id].led);

        /* Check validity of the new data */
        if(motor_feedback[id].checksum == checksum){
            LOG_INFO("Receive feedback from motor %d", id);
        }
        else{
            LOG_ERR("Feedback from motor %d is invalid, CRC 0x%04X != CAL 0x%04X", id, motor_feedback[id].checksum, checksum);
        }
        motor_reception[id].index = 0;
    }

    motor_reception[id].prev_byte = byte;
}

/*!
 * @brief  Motor task
 * @param  None
 * @retval None
 */
static void motor_task(void* arg){
    int32_t length;

    while(1){
        /* Motor 1 */
        length = sw_serial_available(sw_serial_motor[MOTOR1_ID]);
        if(length > 0){
            LOG_INFO("Motor %d received %d bytes", MOTOR1_ID + 1, length);
            while(length--){
                motor_parse_command(MOTOR1_ID, sw_serial_read_byte(sw_serial_motor[MOTOR1_ID]));
            }
        }

        /* Motor 2 */
        length = sw_serial_available(sw_serial_motor[MOTOR2_ID]);
        if(length > 0){
            LOG_INFO("Motor %d received %d bytes", MOTOR2_ID + 1, length);
            while(length--){
                motor_parse_command(MOTOR2_ID, sw_serial_read_byte(sw_serial_motor[MOTOR2_ID]));
            }
        }
        vTaskDelay(100 / portTICK_RATE_MS);
    }
}

/******************************************************************************/

/*!
 * @brief  Control motor
 */
void motor_control(int16_t steer, int16_t speed){
    motor_control_t motor_control;

    motor_control.start = MOTOR_START_FRAME;
    motor_control.steer = steer;
    motor_control.speed = speed;
    motor_control.checksum = motor_control.steer ^ motor_control.speed;

    /* Send command to 2 motors */
    uint8_t *pdata = (uint8_t *) &motor_control;
    for(uint32_t i = 0; i < sizeof(motor_control); i++){
        sw_serial_write_byte(sw_serial_motor[MOTOR1_ID], *pdata);
        sw_serial_write_byte(sw_serial_motor[MOTOR2_ID], *pdata++);
    }
}

/*!
 * @brief  Motor controller initialization
 */
void motor_init(void){
    /* Configure mower pin as output */
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;             /* Disable interrupt */
    io_conf.mode = GPIO_MODE_OUTPUT;                   /* Set as output mode */
    io_conf.pin_bit_mask = 1ULL << MOTOR_MOWER_PIN;    /* Bit mask of the pins that you want to set */
    io_conf.pull_down_en = 1;                          /* Enable pull-down mode */
    io_conf.pull_up_en = 0;                            /* Disable pull-up mode */
    gpio_config(&io_conf);                             /* Configure GPIO with the given settings */

    sw_serial_motor[MOTOR1_ID] = sw_serial_new(MOTOR1_UART_TX_PIN, MOTOR1_UART_RX_PIN,
                                            MOTOR_UART_BAUDRATE, MOTOR_UART_RX_BUFFER_SIZE);
    sw_serial_motor[MOTOR2_ID] = sw_serial_new(MOTOR2_UART_TX_PIN, MOTOR2_UART_RX_PIN,
                                            MOTOR_UART_BAUDRATE, MOTOR_UART_RX_BUFFER_SIZE);

    /* Creat mqtt handle message task */
    BaseType_t result = xTaskCreate(motor_task, MOTOR_TASK_NAME, MOTOR_TASK_SIZE,
                                    NULL, MOTOR_TASK_PRIORITY, NULL);
    if (result != pdPASS) {
        LOG_ERR("Create motor task fail %d", result);
    }

    memset(&motor_reception, 0, sizeof(motor_reception));
    LOG_INFO("Motor controler is initialized");
}
