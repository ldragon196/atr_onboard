/*
 *  sw_serial.h
 *
 *  Created on: Jun 13, 2022
 */

#ifndef _SW_SERIAL_H_
#define _SW_SERIAL_H_

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <driver/gpio.h>

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define SW_SERIAL_ACTIVE_LEVEL                        1
#define SW_SERIAL_INACTIVE_LEVEL                      0

typedef enum {
    SW_SERIAL_BAUDRATE_115200 = 0,
    SW_SERIAL_BAUDRATE_57600,
    SW_SERIAL_BAUDRATE_9600,
    SW_SERIAL_BAUDRATE_INVALID
} sw_serial_baudrate_t;

typedef struct {
    gpio_num_t tx_pin;
    gpio_num_t rx_pin;
    uint32_t buffer_size;
    uint32_t bit_time, rx_start_time, rx_end_time;
    bool overflow;
    volatile uint32_t in_pos, out_pos;
    uint8_t *buffer;
} sw_serial_t;

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/



/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

/*!
 * @brief  Set serial baudrate
 */
esp_err_t sw_serial_set_baudrate(sw_serial_t *serial, sw_serial_baudrate_t baudrate);

/*!
 * @brief  Write a byte
 */
void sw_serial_write_byte(sw_serial_t *serial, uint8_t byte);

/*!
 * @brief  Read a byte
 */
int32_t sw_serial_read_byte(sw_serial_t *serial);

/*!
 * @brief  Read number of available rx bytes
 */
int32_t sw_serial_available(sw_serial_t *serial);

/*!
 * @brief  Create software serial
 */
sw_serial_t *sw_serial_new(gpio_num_t tx_pin, gpio_num_t rx_pin, sw_serial_baudrate_t baudrate, uint32_t buffer_size);

/******************************************************************************/

#endif /* _SW_SERIAL_H_ */
