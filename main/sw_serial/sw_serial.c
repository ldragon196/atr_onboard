/*
 *  sw_serial.c
 *
 *  Created on: Jun 13, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <esp32/clk.h>
#include <driver/gpio.h>
#include <soc/cpu.h>
#include "sw_serial.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define wait_bit_time(wait)     for(uint32_t start = esp_cpu_get_ccount(); esp_cpu_get_ccount() - start < wait;)

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

static bool isr_is_install = false;

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static void IRAM_ATTR sw_serial_rx_handler(void *args);

/******************************************************************************/

/*!
 * @brief  Handle rx data
 */
static void IRAM_ATTR sw_serial_rx_handler(void *args){
    uint8_t rec = 0;
    sw_serial_t *serial = (sw_serial_t *) args;

    portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
    portENTER_CRITICAL(&mux);

    wait_bit_time(serial->rx_start_time);
    if(gpio_get_level(serial->rx_pin) == 0){
        for (uint8_t i = 0; i != 8; i++){
            rec >>= 1;
            wait_bit_time(serial->bit_time);
            if (gpio_get_level(serial->rx_pin))
            {
                rec |= 0x80;
            }
        }

        /* Wait Start Bit To End */
        wait_bit_time(serial->rx_end_time);
        if (1 == gpio_get_level(serial->rx_pin))
        {
            int next = (serial->in_pos + 1) % serial->buffer_size;
            if (next != serial->out_pos)
            {
                serial->buffer[serial->in_pos] = rec;
                serial->in_pos = next;
            }
            else
            {
                serial->overflow = true;
            }
        }
    }
    portEXIT_CRITICAL(&mux);
}

/******************************************************************************/

/*!
 * @brief  Write a byte
 */
void sw_serial_write_byte(sw_serial_t *serial, uint8_t byte){
    int32_t i, *pdata, value[8];
    for(i = 0; i < 8; i++){
        value[i] = (byte & 1) ? SW_SERIAL_ACTIVE_LEVEL : SW_SERIAL_INACTIVE_LEVEL;
        byte >>= 1;
    }
    pdata = value;

    /* Disable interrupts in order to get a clean transmit */
    portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
    portENTER_CRITICAL(&mux);

    gpio_set_level(serial->tx_pin, SW_SERIAL_INACTIVE_LEVEL);
    wait_bit_time(serial->bit_time);

    for (i = 0; i != 8; i++){
        gpio_set_level(serial->tx_pin, *pdata++);
        wait_bit_time(serial->bit_time);
    }

    /* Stop bit */
    gpio_set_level(serial->tx_pin, SW_SERIAL_ACTIVE_LEVEL);
    wait_bit_time(serial->bit_time);

    /* Re-enable interrupts */
    portEXIT_CRITICAL(&mux);
}

/*!
 * @brief  Read a byte
 */
int32_t sw_serial_read_byte(sw_serial_t *serial){
    if(serial->in_pos != serial->out_pos){
        uint8_t ch = serial->buffer[serial->out_pos];
        serial->out_pos = (serial->out_pos + 1) % serial->buffer_size;
        return ch;
    }
    return -1;
}

/*!
 * @brief  Read number of available rx bytes
 */
int32_t sw_serial_available(sw_serial_t *serial){
    int32_t available = serial->in_pos - serial->out_pos;
    return (available < 0) ? available + serial->buffer_size : available;
}

/*!
 * @brief  Set serial baudrate
 */
esp_err_t sw_serial_set_baudrate(sw_serial_t *serial, sw_serial_baudrate_t baudrate){
    switch (baudrate){
        case SW_SERIAL_BAUDRATE_115200:
            serial->bit_time = (esp_clk_cpu_freq() / 115200);
            serial->rx_start_time = (serial->bit_time / 256);
            serial->rx_end_time = (serial->bit_time * 127 / 256);
            break;
        
        case SW_SERIAL_BAUDRATE_57600:
            serial->bit_time = (esp_clk_cpu_freq() / 57600);
            serial->rx_start_time = (serial->bit_time / 9);
            serial->rx_end_time = (serial->bit_time * 8 / 9);
            break;

        case SW_SERIAL_BAUDRATE_9600:
            serial->bit_time = (esp_clk_cpu_freq() / 9600);
            serial->rx_start_time = (serial->bit_time / 9);
            serial->rx_end_time = (serial->bit_time * 8 / 9);
            break;

        default:
            return ESP_FAIL;
    }
    return ESP_OK;
}

/*!
 * @brief  Create software serial
 */
sw_serial_t *sw_serial_new(gpio_num_t tx_pin, gpio_num_t rx_pin, sw_serial_baudrate_t baudrate, uint32_t buffer_size){
    sw_serial_t *serial = (sw_serial_t *) malloc(sizeof(sw_serial_t));
    if(serial != NULL){
        serial->overflow = false;
        serial->in_pos = 0;
        serial->out_pos = 0;
        serial->buffer = (uint8_t *) malloc(buffer_size);
        if(serial->buffer != NULL){
            serial->rx_pin = rx_pin;
            gpio_pad_select_gpio(rx_pin);
            gpio_set_direction(rx_pin, GPIO_MODE_INPUT);

            serial->tx_pin = tx_pin;
            gpio_pad_select_gpio(tx_pin);
            gpio_set_direction(tx_pin, GPIO_MODE_OUTPUT);

            /* For the TTL level of positive logic, the starting bit is the low level of one bit time */
            gpio_set_level(tx_pin, SW_SERIAL_ACTIVE_LEVEL);
            vTaskDelay(10 / portTICK_RATE_MS);

            /* Enable interrupt for RX */
            gpio_set_intr_type(serial->rx_pin, SW_SERIAL_ACTIVE_LEVEL ? GPIO_INTR_NEGEDGE : GPIO_INTR_POSEDGE);
            if(!isr_is_install){
                isr_is_install = true;
                gpio_install_isr_service(0);
            }
            gpio_isr_handler_add(serial->rx_pin, sw_serial_rx_handler, (void*) serial);

            /* Set baudrate */
            sw_serial_set_baudrate(serial, baudrate);
            return serial;
        }
        free(serial);
    }
    return serial;
}