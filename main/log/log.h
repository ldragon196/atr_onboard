/*
 *  log.h
 *
 *  Created on: Jun 7, 2022
 */

#ifndef _LOG_H_
#define _LOG_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LLOG_LEVEL_NONE                            0    /*< No log */
#define LLOG_LEVEL_ERR                             1    /*< Errors */
#define LLOG_LEVEL_WARN                            2    /*< Warnings */
#define LLOG_LEVEL_INFO                            3    /*< Basic info */
#define LLOG_LEVEL_DBG                             4    /*< Detailled debug */

/* Log coloring */
#define TC_RESET                                   "\033[0m"
#define TC_BLACK                                   "\033[0;30m"
#define TC_RED                                     "\033[0;31m"
#define TC_GREEN                                   "\033[0;32m"
#define TC_YELLOW                                  "\033[0;33m"
#define TC_BLUE                                    "\033[0;34m"
#define TC_MAGENTA                                 "\033[0;35m"
#define TC_CYAN                                    "\033[0;36m"
#define TC_WHITE                                   "\033[0;37m"

#define LOG_COLOR_RESET                            TC_RESET

#ifndef LOG_COLOR_ERR
#define LOG_COLOR_ERR                              TC_RED
#endif

#ifndef LOG_COLOR_WARN
#define LOG_COLOR_WARN                             TC_YELLOW
#endif

#ifndef LOG_COLOR_INFO
#define LOG_COLOR_INFO                             TC_BLUE
#endif

#ifndef LOG_COLOR_DBG
#define LOG_COLOR_DBG                              TC_WHITE
#endif

#ifndef LOG_COLOR_PRI
#define LOG_COLOR_PRI                              TC_CYAN
#endif

#if QTT_LOG_OUTPUT
#include "mqtt_api.h"
extern char log_mqtt[1024];
extern uint32_t log_index;
#endif

#define LOG_WITH_COLOR                             0
#define LOG_WITH_MODULE_PREFIX                     0
#define LOG_WITH_LOC                               1

#if QTT_LOG_OUTPUT
#define LOG_OUTPUT(...)                            do{printf(__VA_ARGS__); log_index += sprintf(&log_mqtt[log_index], __VA_ARGS__);}while(0)
#else
#define LOG_OUTPUT(...)                            printf(__VA_ARGS__)
#endif

#define LOG_OUTPUT_PREFIX(level, levelstr, module) LOG_OUTPUT("[%-4s: %-10s] ", levelstr, module)
#define LOG_OUTPUT_NONEPREFIX(level, levelstr)     LOG_OUTPUT("[%-4s] ", levelstr)
#define __FILE_NAME__                              (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#if QTT_LOG_OUTPUT
#define LOG(newline, level, levelstr, levelcolor, ...)             \
    do {                                                           \
        log_index = 0;                                             \
        if (level <= (LOG_LEVEL)) {                                \
            if (newline) {                                         \
                if (LOG_WITH_COLOR) {                              \
                     LOG_OUTPUT(levelcolor);                       \
                }                                                  \
                if (LOG_WITH_MODULE_PREFIX) {                      \
                    LOG_OUTPUT_PREFIX(level, levelstr, LOG_MODULE);\
                }                                                  \
                else {                                             \
                    LOG_OUTPUT_NONEPREFIX(level, levelstr);        \
                }                                                  \
                if (LOG_WITH_LOC) {                                \
                    char loc_str[64];                              \
                    snprintf(loc_str, sizeof(loc_str), "%s:%d", __FILE_NAME__, __LINE__); \
                    LOG_OUTPUT("[%-28s] ", loc_str);               \
                }                                                  \
                if (LOG_WITH_COLOR) {                              \
                    LOG_OUTPUT(LOG_COLOR_RESET);                   \
                }                                                  \
            }                                                      \
            LOG_OUTPUT(__VA_ARGS__);                               \
            LOG_OUTPUT("\r\n");                                    \
        }                                                          \
        mqtt_api_publish(MQTT_LOG_TOPIC, log_mqtt, 0, true);       \
    } while (0)
#else
#define LOG(newline, level, levelstr, levelcolor, ...)             \
    do {                                                           \
        if (level <= (LOG_LEVEL)) {                                \
            if (newline) {                                         \
                if (LOG_WITH_COLOR) {                              \
                     LOG_OUTPUT(levelcolor);                       \
                }                                                  \
                if (LOG_WITH_MODULE_PREFIX) {                      \
                    LOG_OUTPUT_PREFIX(level, levelstr, LOG_MODULE);\
                }                                                  \
                else {                                             \
                    LOG_OUTPUT_NONEPREFIX(level, levelstr);        \
                }                                                  \
                if (LOG_WITH_LOC) {                                \
                    char loc_str[64];                              \
                    snprintf(loc_str, sizeof(loc_str), "%s:%d", __FILE_NAME__, __LINE__); \
                    LOG_OUTPUT("[%-28s] ", loc_str);               \
                }                                                  \
                if (LOG_WITH_COLOR) {                              \
                    LOG_OUTPUT(LOG_COLOR_RESET);                   \
                }                                                  \
            }                                                      \
            LOG_OUTPUT(__VA_ARGS__);                               \
            LOG_OUTPUT("\r\n");                                    \
        }                                                          \
    } while (0)
#endif

#define LOG_PRINT(...)                             LOG(1, 0, "PRI", LOG_COLOR_PRI, __VA_ARGS__)
#define LOG_ERR(...)                               LOG(1, LLOG_LEVEL_ERR, "ERR", LOG_COLOR_ERR, __VA_ARGS__)
#define LOG_WARN(...)                              LOG(1, LLOG_LEVEL_WARN, "WARN", LOG_COLOR_WARN, __VA_ARGS__)
#define LOG_INFO(...)                              LOG(1, LLOG_LEVEL_INFO, "INFO", LOG_COLOR_INFO, __VA_ARGS__)
#define LOG_DBG(...)                               LOG(1, LLOG_LEVEL_DBG, "DBG", LOG_COLOR_DBG, __VA_ARGS__)


#ifdef __cplusplus
}
#endif

#endif /* _LOG_H_ */
