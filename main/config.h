/*
 *  config.h
 *
 *  Created on: Jun 7, 2022
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

/* Info */
#define DEVICE_NAME                                   "esp32_gps"
#define FIRMWARE_VERSION                              "1.0.0"
#define HARDWARE_VERSION                              "1.0.0"
#define OTA_VERSION                                   1

/* Log level */
#define QTT_LOG_OUTPUT                                1    /* Enable publish log to MQTT topic */
#define LOG_LEVEL_APP_MAIN                            LLOG_LEVEL_DBG
#define LOG_LEVEL_WIFI                                LLOG_LEVEL_DBG
#define LOG_LEVEL_OTA                                 LLOG_LEVEL_DBG
#define LOG_LEVEL_NTRIP                               LLOG_LEVEL_WARN
#define LOG_LEVEL_MQTT                                LLOG_LEVEL_ERR
#define LOG_LEVEL_MOTOR                               LLOG_LEVEL_DBG
#define LOG_LEVEL_GSM                                 LLOG_LEVEL_WARN
#define LOG_LEVEL_GPS                                 LLOG_LEVEL_WARN

/* Wifi */
#define WIFI_SSID                                     "ESP32"
#define WIFI_PASSWORD                                 "ratatouille"
#define WIFI_SSID_MAX_LENGTH                          32
#define WIFI_PASSWORD_MAX_LENGTH                      64
#define WIFI_TIME_RETRY_CONNECT_MS                    3000

/* GSM module */
#define MODEM_UART_NUM                                UART_NUM_1
#define MODEM_UART_TX_PIN                             27
#define MODEM_UART_RX_PIN                             26
#define MODEM_UART_RTS_PIN                            5
#define MODEM_UART_CTS_PIN                            -1
#define MODEM_UART_BAUDRATE                           115200

#define MODEM_PWRKEY_PIN                              4
#define MODEM_POWER_PIN                               23

#define MODEM_UART_RX_BUFFER_SIZE                     1024
#define MODEM_UART_TX_BUFFER_SIZE                     512
#define MODEM_UART_PATTERN_QUEUE_SIZE                 20
#define MODEM_UART_EVENT_QUEUE_SIZE                   30
#define MODEM_UART_EVENT_TASK_STACK_SIZE              4096
#define MODEM_UART_EVENT_TASK_PRIORITY                5

#define MODEM_PPP_AUTH_NONE
#ifndef MODEM_PPP_AUTH_NONE
    #define MODEM_PPP_AUTH_USERNAME                   ""
    #define MODEM_PPP_AUTH_PASSWORD                   ""
#endif

/* MQTT */
#define MQTT_DATA_MAX_LENGTH                          1024
#define MQTT_TOPIC_MAX_LENGTH                         128
#define MQTT_MAX_SUBCRIBE_TOPIC                       8
#define MQTT_CLIENT_ID_LENGTH                         32
#define MQTT_MESSAGE_QUEUE_SIZE                       4
#define MQTT_QUEUE_MAX_DELAY_MS                       200

#define MQTT_BROKER_URI                               "mqtts://broker.emqx.io:8883"
#define MQTT_COMMAND_TOPIC                            "gps/command/"
#define MQTT_OTA_TOPIC                                "device/ota"
#define MQTT_GPS_TOPIC                                "gps/status/"
#define MQTT_HEARTBEAT_TOPIC                          "device/heartbeat"
#define MQTT_LOG_TOPIC                                "device/log"

/* GPS */
#define GPS_UART_NUM                                  UART_NUM_2
#define GPS_UART_TX_PIN                               21
#define GPS_UART_RX_PIN                               22
#define GPS_UART_BAUDRATE                             115200

#define TIME_HANDLE_GPS_MS                            3000

/* Ntrip client */
#define NTRIP_CLIENT_HOST                             "3.23.52.207"
#define NTRIP_CLIENT_PORT                             2101
#define NTRIP_CLIENT_MOUNTPOINT                       "/MGPLUS"
#define NTRIP_CLIENT_USERNAME                         "rtk@rospers.com"
#define NTRIP_CLIENT_PASSWORD                         ""
#define NTRIP_POLLING_TIME_MS                         5000

/* Motor */
#define MOTOR_MOWER_LEVEL                             1
#define MOTOR_UART_BAUDRATE                           SW_SERIAL_BAUDRATE_115200

#define MOTOR_MOWER_PIN                               15
#define MOTOR1_UART_TX_PIN                            32
#define MOTOR1_UART_RX_PIN                            33
#define MOTOR2_UART_TX_PIN                            4
#define MOTOR2_UART_RX_PIN                            2

/* Task */
#define MQTT_TASK_NAME                                "MQTT"
#define MQTT_TASK_SIZE                                4096
#define MQTT_TASK_PRIORITY                            4

#define MOTOR_TASK_NAME                               "MOTOR"
#define MOTOR_TASK_SIZE                               4096
#define MOTOR_TASK_PRIORITY                           5

#define GPS_TASK_NAME                                 "GPS"
#define GPS_TASK_SIZE                                 4096
#define GPS_TASK_PRIORITY                             5

#define HTTP_OTA_TASK_NAME                             "OTA"
#define HTTP_OTA_TASK_SIZE                             4096
#define HTTP_OTA_TASK_PRIORITY                         5

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/



/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/



/******************************************************************************/

#endif /* _CONFIG_H_ */
