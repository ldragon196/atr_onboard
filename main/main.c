/*
 *  main.c
 *
 *  Created on: Jun 7, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_ota_ops.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <esp_log.h>
#include <cJSON.h>
#include "config.h"
#include "log.h"
#include "ntrip_client.h"
#include "gps_api.h"
#include "gsm_api.h"
#include "wifi_lib.h"
#include "mqtt_api.h"
#include "motor.h"
#include "ota_api.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define GPS_ENABLED      1
#define GSM_ENABLED      1
#define MOTOR_ENABLED    1

#define JSON_CHECK(a, str, ...) \
    if (!(a)) { LOG_ERR("%s(%u): " str, __FUNCTION__, __LINE__, ##__VA_ARGS__); goto error; }

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "MAIN"
#define LOG_LEVEL                                  LOG_LEVEL_APP_MAIN
char mqtt_heartbeat[256];

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/



/******************************************************************************/

/*!
 * @brief  Handle gps command received from mqtt
 */
void main_handle_mqtt_gps_cmd(char *message, uint32_t length){
    cJSON* item;
    cJSON* root = cJSON_Parse(message);
    JSON_CHECK((root != NULL), "Couldn't parse message json");

    /* Get commamd */
    item = cJSON_GetObjectItem(root, "ntrip");
    JSON_CHECK(cJSON_IsString(item), "Couldn't parse command json");

    ntrip_config_t config;
    item = cJSON_GetObjectItem(root, "host");
    JSON_CHECK(cJSON_IsString(item), "Can't parse ntrip server host json");
    sprintf(config.host, "%s", item->valuestring);

    item = cJSON_GetObjectItem(root, "port");
    JSON_CHECK(cJSON_IsNumber(item), "Can't parse ntrip server port json");
    config.port = item->valueint;

    item = cJSON_GetObjectItem(root, "mountpoint");
    JSON_CHECK(cJSON_IsString(item), "Can't parse ntrip server mountpoint json");
    sprintf(config.mountpoint, "%s", item->valuestring);

    item = cJSON_GetObjectItem(root, "username");
    JSON_CHECK(cJSON_IsString(item), "Can't parse ntrip server username json");
    sprintf(config.username, "%s", item->valuestring);

    item = cJSON_GetObjectItem(root, "password");
    JSON_CHECK(cJSON_IsString(item), "Can't parse ntrip server password json");
    sprintf(config.password, "%s", item->valuestring);

    /* Change ntrip configuration */
    ntrip_client_configuration(&config);

/* If parsing have any error */
error:
    cJSON_Delete(root);
}

/*!
 * @brief  Handle mqtt OTA request
 */
void main_handle_mqtt_ota(char *message, uint32_t length){
    cJSON* item;
    cJSON* root = cJSON_Parse(message);
    JSON_CHECK((root != NULL), "Couldn't parse message json");

    /* Get commamd */
    item = cJSON_GetObjectItem(root, "ota");
    JSON_CHECK(cJSON_IsString(item), "Couldn't parse ota command json");

    ota_info_t ota_info;
    item = cJSON_GetObjectItem(root, "url");
    JSON_CHECK(cJSON_IsString(item), "Can't parse url json");
    sprintf(ota_info.file_path, "%s", item->valuestring);

    item = cJSON_GetObjectItem(root, "version");
    JSON_CHECK(cJSON_IsNumber(item), "Can't parse version json");
    ota_info.version = item->valueint;

    /* Check current version */
    if(ota_info.version != OTA_VERSION){
        ota_api_request_ota(&ota_info);
    }
    else{
        LOG_WARN("OTA request with same version");
    }

/* If parsing have any error */
error:
    cJSON_Delete(root);
}

/******************************************************************************/

/* Startup function */
void app_main(void){
    LOG_INFO("Power up! Firmware version %s, hardware version %s", FIRMWARE_VERSION, HARDWARE_VERSION);

    /* init nvs flash */
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        LOG_ERR("nvs_flash_init err %04x", err);
        /* NVS partition was truncated and needs to be erased */
        ESP_ERROR_CHECK(nvs_flash_erase());
        /* Retry nvs_flash_init */
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

#if GSM_ENABLED
    /* Initialize GSM module to connect to internet */
    esp_netif_t *esp_netif = gsm_api_init();
    while(esp_netif == NULL){
        LOG_INFO("Couldn't initialize GSM module");
        vTaskDelay(5000 / portTICK_RATE_MS);
    }
#else
    /* Start wifi station mode */
    wifi_lib_init_sta();

    /* Task will suppend at here util network up */
    wifi_lib_wait_network_up();
#endif

    /* MQTT initialization */
    mqtt_register_callback(MQTT_COMMAND_TOPIC, main_handle_mqtt_gps_cmd);
    mqtt_register_callback(MQTT_OTA_TOPIC, main_handle_mqtt_ota);
    mqtt_api_init();

#if GPS_ENABLED
    gps_message_t gps_message;

    /* Initialize GPS */
    gps_api_init();

    /* Initialize ntrip client */
    ntrip_client_init();
#endif

#if MOTOR_ENABLED
    /* Initialize motor controler */
    motor_init();

    motor_control(100, 100);
#endif

    /* Update OTA init */
    ota_api_init();

    /* Main loop */
    uint32_t main_count = 0;
    while(1){
    #if GPS_ENABLED
        if(gps_api_get_message(&gps_message, 1000)){
            LOG_INFO("GPS Message %s", gps_message.message);
            ntrip_client_send((uint8_t *) gps_message.message, strlen(gps_message.message));

            /* Publish to MQTT broker */
            mqtt_api_publish(MQTT_GPS_TOPIC, gps_message.message, MQTT_AUTO_LENGTH, true);

            main_count++;
            LOG_INFO("Main cycle %u, Free heap %u", main_count, esp_get_minimum_free_heap_size());
        }
    #endif

        /* Publish heartbeat message */
        sprintf(mqtt_heartbeat, "{\"device\":\"%s\",\"ota_ver\":\"%d\"", DEVICE_NAME, OTA_VERSION);
        mqtt_api_publish(MQTT_HEARTBEAT_TOPIC, mqtt_heartbeat, MQTT_AUTO_LENGTH, false);

        vTaskDelay(1000 / portTICK_RATE_MS);
        main_count++;
        LOG_INFO("Main cycle %u, Free heap %u", main_count, esp_get_minimum_free_heap_size());
    }
}
