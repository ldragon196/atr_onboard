/*
 *  gps_api.h
 *
 *  Created on: Jun 8, 2022
 */

#ifndef _GPS_API_H_
#define _GPS_API_H_

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/



/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define GPS_UART_RX_BUFF_SIZE                         1024
#define GPS_MESSAGE_LENGTH                            128
#define GPS_MESSAGE_QUEUE_SIZE                        8

typedef struct {
    char message[GPS_MESSAGE_LENGTH];
} gps_message_t;

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/



/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

/*!
 * @brief  Get GPS messages from queue
 * @param  [out] GPS message
 *         [in] timeout ms
 * @retval True if success
 */
bool gps_api_get_message(gps_message_t *message, uint32_t delay_ms);

/**
 * @brief Write NMEA command
 * @return esp_err_t
 *  - ESP_OK: Success
 *  - ESP_ERR_INVALIG_ARG: Invalid combination of event base and event id
 *  - Others: Fail
 */
esp_err_t gps_api_send_cmd(const char *fmt, ...);

/**
 * @brief  Send bytes via UART
 * @param  Data and length of data
 * @retval None
 */
void gps_api_send_bytes(uint8_t *data, size_t length);

/*!
 * @brief  GPS module initialization
 * @param  None
 * @retval None
 */
void gps_api_init(void);

/******************************************************************************/

#endif /* _GPS_API_H_ */
