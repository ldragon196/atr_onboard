/*
 *  gps_api.c
 *
 *  Created on: Jun 8, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <driver/uart.h>
#include "config.h"
#include "log.h"
#include "gps_api.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/



/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "GPS"
#define LOG_LEVEL                                  LOG_LEVEL_GPS

static QueueHandle_t gps_message_queue;
static uint8_t gps_rx_data[GPS_UART_RX_BUFF_SIZE + 1];

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/



/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static uint8_t gps_api_checksum(char *sentence);
static int32_t gps_api_nmea_vasprintf(char **strp, const char *fmt, va_list args);
static void gps_api_task(void* arg);

/******************************************************************************/

/**
 * @brief Calculate NMEA checksum
 */
static uint8_t gps_api_checksum(char *sentence){
    uint8_t checksum = 0;
    unsigned int length = strlen(sentence);
    for(unsigned int i = 1; i < length; i++){
        checksum ^= (uint8_t) sentence[i];
    }

    return checksum;
}

/**
 * @brief Create NMEA format
 */
static int32_t gps_api_nmea_vasprintf(char **strp, const char *fmt, va_list args){
    char *sentence;
    vasprintf(&sentence, fmt, args);
    uint8_t checksum = gps_api_checksum(sentence);
    int l = asprintf(strp, "%s*%02X\r\n", sentence, checksum);
    free(sentence);

    return l;
}

/*! 
 * @brief  GPS receive task
 * @param  None
 * @retval None
 */
static void gps_api_task(void* arg){
    int32_t length = 0, index = 0xFFFFFF;
    uint8_t *pdata;
    gps_message_t gps_message;
    uint32_t last_time_gps = xTaskGetTickCount();

    while(1){
        length = uart_read_bytes(GPS_UART_NUM, gps_rx_data, GPS_MESSAGE_LENGTH, 1000 / portTICK_RATE_MS);
        if(length > 0){
            pdata = gps_rx_data;
            while(length--){
                /* Start of a statement */
                if(*pdata == '$'){
                    index = 0;
                }

                /* End of a statement */
                else if (*pdata == '\n'){
                    if(xTaskGetTickCount() - last_time_gps < TIME_HANDLE_GPS_MS / portTICK_RATE_MS){
                        continue;
                    }

                    last_time_gps = xTaskGetTickCount();
                    gps_message.message[index] = '\0';
                    if((!strncmp(gps_message.message, "GNGGA", strlen("GNGGA"))) ||
                       (!strncmp(gps_message.message, "GNRMC", strlen("GNGGA")))){
                        /* Send to queue */
                        if(xQueueSend(gps_message_queue, &gps_message, MQTT_QUEUE_MAX_DELAY_MS) != pdTRUE){
                            LOG_WARN("Send to GPS message queue fail");
                        }
                    }
                }

                /* Fill data */
                else if(index < GPS_MESSAGE_LENGTH){
                    gps_message.message[index++] = *pdata;
                }

                pdata++;
            }
        }
    }
}

/******************************************************************************/

/*!
 * @brief  Get GPS messages from queue
 */
bool gps_api_get_message(gps_message_t *message, uint32_t delay_ms){
    return (bool) xQueueReceive(gps_message_queue, message, delay_ms / portTICK_PERIOD_MS);
}

/**
 * @brief Write NMEA command
 */
esp_err_t gps_api_send_cmd(const char *fmt, ...){
    esp_err_t ret = ESP_OK;
    va_list args;
    va_start(args, fmt);

    char *nmea;
    gps_api_nmea_vasprintf(&nmea, fmt, args);

    /* Send data to GPS */
    int l = uart_write_bytes(GPS_UART_NUM, nmea, strlen(nmea));
    if(l < 0){
        ret = ESP_FAIL;
    }
    free(nmea);
    va_end(args);

    return ret;
}

/**
 * @brief  Send bytes via UART
 */
void gps_api_send_bytes(uint8_t *data, size_t length){
    uart_write_bytes(GPS_UART_NUM, data, length);
}

/*!
 * @brief  GPS module initialization
 * @param  None
 * @retval None
 */
void gps_api_init(void){
    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = GPS_UART_BAUDRATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };

    /* We won't use a buffer for sending data */
    uart_driver_install(GPS_UART_NUM, GPS_UART_RX_BUFF_SIZE << 1, 0, 0, NULL, 0);
    uart_param_config(GPS_UART_NUM, &uart_config);
    uart_set_pin(GPS_UART_NUM, GPS_UART_TX_PIN, GPS_UART_RX_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    /* Creat queue for GPS messages */
    gps_message_queue = xQueueCreate(GPS_MESSAGE_QUEUE_SIZE, sizeof(gps_message_t));
    if(gps_message_queue == NULL)
    {
        LOG_ERR("Create message queue fail");
        return;
    }

    /* Create GPS task */
    BaseType_t result = xTaskCreate(gps_api_task, GPS_TASK_NAME, GPS_TASK_SIZE,
                                    NULL, GPS_TASK_PRIORITY, NULL);
    if(result != pdPASS){
        LOG_ERR("Create GPS task fail %d", result);
    }
    LOG_INFO("GPS is initialized");
}
