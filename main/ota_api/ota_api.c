/*
 *  ota_api.c
 *
 *  Created on: Aug 7, 2022
 */

/******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <sys/cdefs.h>
#include "esp_netif.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include <esp_log.h>
#include "config.h"
#include "log.h"
#include "ota_api.h"

/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

#define HTTPS_USED 0

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

#define LOG_MODULE                                 "OTA"
#define LOG_LEVEL                                  LOG_LEVEL_OTA

static QueueHandle_t ping_ota_queue;               /**< Queue for request OTA */

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/

#if HTTPS_USED
extern const uint8_t http_server_cert_pem_start[] asm("_binary_https_ca_cert_pem_start");
extern const uint8_t http_server_cert_pem_end[] asm("_binary_https_ca_cert_pem_end");
#endif

/******************************************************************************/
/*                                FUNCTIONS                                   */
/******************************************************************************/

static esp_err_t http_event_handler(esp_http_client_event_t* event);
static void ota_api_flash_task(void* arg);

/******************************************************************************/

/*!
 * @brief  Http event handler
 * @param  None
 * @retval None
 */
static esp_err_t http_event_handler(esp_http_client_event_t* event){
    switch (event->event_id) {
    case HTTP_EVENT_ERROR:
        LOG_INFO("HTTP_EVENT_ERROR");
        break;

    case HTTP_EVENT_ON_CONNECTED:
        LOG_INFO("HTTP_EVENT_ON_CONNECTED");
        break;

    case HTTP_EVENT_HEADER_SENT:
        LOG_INFO("HTTP_EVENT_HEADER_SENT");
        break;

    case HTTP_EVENT_ON_HEADER:
        LOG_INFO("HTTP_EVENT_ON_HEADER, key=%s, value=%s", event->header_key, event->header_value);
        break;

    case HTTP_EVENT_ON_DATA:
        // LOG_INFO("HTTP_EVENT_ON_DATA, len=%d", event->data_len);
        break;

    case HTTP_EVENT_ON_FINISH:
        LOG_INFO("HTTP_EVENT_ON_FINISH");
        break;

    case HTTP_EVENT_DISCONNECTED:
        LOG_INFO("HTTP_EVENT_DISCONNECTED");
        break;
    }

    return ESP_OK;
}

/*!
 * @brief  OTA task
 * @param  None
 * @retval None
 */
static void ota_api_flash_task(void* arg){
    ota_info_t ota_info;

    while(1){
        /* Wait request pin to https server for OTA from mqtt_api */
        xQueueReceive(ping_ota_queue, &ota_info, portMAX_DELAY);

        LOG_INFO("*********************** Request OTA to %s, version %u", ota_info.file_path, ota_info.version);
        esp_http_client_config_t config = {
            .url = ota_info.file_path,
    #if HTTPS_USED
            .cert_pem = (const char*) http_server_cert_pem_start,
    #endif
            .event_handler = http_event_handler,
            .keep_alive_enable = true,
        };

        /* Start OTA */
        LOG_ERR("START Firmware upgrade");
        esp_err_t ret = esp_https_ota(&config);
        if(ret == ESP_OK){
            LOG_INFO("New Reboot started");
            vTaskDelay(1000 / portTICK_RATE_MS);
            /* reset the device */
            esp_restart();
        }
        else{
            LOG_ERR("Firmware upgrade failed");
        }
        LOG_ERR("STOP Firmware upgrade");
        
        vTaskDelay(10000 / portTICK_RATE_MS);
    }
}

/******************************************************************************/

/*!
 * @brief  Start ota update with default url
 */
void ota_api_test(void){
    ota_info_t mqtt_ota_info = {
#if HTTPS_USED
        .file_path = "https://192.168.0.108:8884/firmware.bin",
#else
        .file_path = "http://192.168.0.108:8884/firmware.bin",
#endif
        .version = 1
    };
    xQueueOverwrite(ping_ota_queue, &mqtt_ota_info);
}

/*!
 * @brief  Request update OTA
 */
void ota_api_request_ota(ota_info_t *ota_info){
    xQueueOverwrite(ping_ota_queue, ota_info);
}

/*!
 * @brief  Start http client for OTA
 */
void ota_api_init(void){
    /* Creat queue for request ota and mqtt info */
    ping_ota_queue = xQueueCreate(1, sizeof(ota_info_t));

    BaseType_t result = xTaskCreate(ota_api_flash_task, HTTP_OTA_TASK_NAME, HTTP_OTA_TASK_SIZE,
                                    NULL, HTTP_OTA_TASK_PRIORITY, NULL);
    if (result != pdPASS) {
        LOG_ERR("Create ota task fail %d", result);
    }
    // ota_api_test();
}