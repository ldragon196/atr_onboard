# Creat http/https server for OTA
1. Install nodejs https://nodejs.org/en/download/

2. Install http-server
    $ npm install --global http-server

3. Creat folder for http server and run Command Prompt
After build project, copy firmware file (./build/project_name.bin) to this folder

4. Creat key and cert with openssl (for https)
When filling out the form the common name is important and is usually the domain name of the server
    $ openssl req -x509 -newkey rsa:2048 -keyout ca_key.pem -out ca_cert.pem -days 365 -nodes

5. Start https server on port (ex: 8884)
    $ http-server -S -p 8884 -K ca_key.pem -C ca_cert.pem -o    (https)
    $ http-server -p 8884                                       (http)

6. Send OTA request via Mqtt, topic "device/ota"
- Public to ota topic (see config.h)
- Message example
    {
        "ota":"update",
        "url":"http://192.168.1.100:8884/firmware.bin",
        "version":1
    }